package by.agh

fun main(args: Array<String>) {
    // shl - сдвиг числа со знаком влево
    val z = 3 shl 2 //z = 11 << 2 = 1100
    println(z) // z = 12

    val d = 0b11 shl 2
    println(d) // d =12

    // shr - сдвиг числа со знаком вправо
    val z2 = 12 shr 2 // z = 1100 >> 2 =11
    println(z2) // z = 3
    val d2 = 0b1100 shr 2
    println(d2) // d =3

    // ushr - сдвиг битов беззнакового числа вправо
    val z3 = 12 ushr 2 // z = 1100 >> 2 = 11
    println(z3) //z = 3

    //and - AND логическое умножение или конъюнкция
    val x = 5 //101
    val y = 6 //110
    val z4 = x and y //z = 101 & 110 = 100
    println(z4)

    val d3 = 0b101 and 0b110
    println(d3)

    //or - побитовая операция OR, логическое сложение или дизъюнкция
    val x5 = 5 //101
    val y5 = 6 //110
    val z5 = x5 or y5 // z = 101 | 110 = 111
    println(z5) // z - 7

    val d5 = 0b101 or 0b110
    println(d5) // d = 7

    // XOR
    val x6 = 5 // 101
    val y6 = 6 // 110
    val z6 = x6 xor y6 // z = 101 ^ 110 = 011
    println(z6) // z = 3

    val d6 = 0b101 xor 0b110
    println(d6) // d = 3

    // inv логическое отрицание или инверсия
    val b = 11 //1011
    val c = b.inv()
    println(c) // -12


    //логические операции and/&& or/||
    val q = (11 >= 5) and (9 < 10)
    println(y)

    val r = true xor (90 > 10) // false
    println(r)

    val tt = true
    val u = !tt  // false
    val o = !u   // true

    val u2 = tt.not() // false
    val o2 = u2.not() // // true

    val aa = 5
    val bb = aa in 1..6 // true

    val num = 10
    when (num) {
        in 10..19 -> println("10-19")
        in 20..29 -> println("20-29")
        !in 10..20 -> println("not in 10-19")
        else -> println("not defined")
    }

    val sum = 1000
    val rate = when (sum) {
        in 100..999 -> 10
        in 1000..9999 -> 15
        else -> 20
    }

    println(rate)

    for (n in 1..9) {
        print("${n * n}\t")
    }

    println("\n")

    for (i in 1..9) {
        for (j in 1..9) {
            print("${i * j} \t")
        }
        println()
    }

    var range = 1..5
    var rangeD = 5 downTo 1
    range.forEach { print(it) }

    var range2 = "a".."d"
    var range3 = 1..10 step 2 // 1 3 5 7 9
    var range4 = 10 downTo 1 step 3 // 10 7 4 1

    var rangeUntil = 1 until 9 // 1 2 3 4 5 6 7 8
    var rangeUntil2 = 1 until 9 step 2 // 1 3 5 7

    val isInRange = 86 in range //false

    val numbers: Array<Int> = arrayOf(1, 2, 3, 4, 5)

    val numbers2 = Array(3) { 5 } // [5,5,5]
    numbers2.forEach { print(it) }

    val ints: IntArray = intArrayOf(1, 2, 3, 4, 5)
    val doubles: DoubleArray = doubleArrayOf(2.4, 5.6, 2.3)


    /// 2d arrays
    val table: Array<Array<Int>> = Array(3) { Array(5) { 0 } }
    table[0] = arrayOf(1, 2, 3)
    table[1] = arrayOf(4, 5, 6)
    table[2] = arrayOf(7, 8, 9)
    table[0][1] = 6
    val n = table[0][1]
    println("\n $n")

    for (row in table) {
        for (cell in row) {
            print("$cell \t")
        }
        println()
    }

    val phones: Array<String> = arrayOf("Galaxy 7", "Iphone", "Motorola")
    for (phone in phones) {
        println(phone)
    }

    val name : String = "Tom"
    val id: Int? = name as? Int
    println(id) //null
}