package by.agh

fun main(args: Array<String>) {
    dataTypes()
    controlFlow()
}

fun controlFlow() {
    val a: Int = 5
    val b: Int = 2
    var max: Int
    if (a > b) {
        max = a
    } else {
        max = b
    }

    println("Maximum of: $max")
    val max2 = if (a > b) a else b
    println(max2)

    val x: Int = 5
    when (x) {
        /*1 -> println("x==1")
        2 -> println("x==2")*/
        1, 2 -> println("value of x either 1 or 2")
        else -> {
            println("x is neither 1 nor 2")
        }
    }

    val items = listOf(1, 2, 3, 4)
    for (i in items) println("values of the array $i")

    val items2 = listOf(1, 22, 83, 4)
    for ((index, value) in items2.withIndex()) {
        println("the element at $index is $value")
    }

    var z:Int = 0
    println("Example of while loop --")
    while (z<=10)
    {
        println(z++)
    }

    var z2: Int = 0;
    do {
        z2 += 10
        println("I'm inside do block---" +z2)
    }while (z2<=50)

    var doubled:Int = 10
    println("The value of X is--" + doubleMe(doubled))

    println("Example of Break and Continue ----")
    myCustomLabel@ for (x in 1..10) {
        if (x==5){
            println("i am inside if block with value $x \n")
            break@myCustomLabel
        }else{
            println("i am inside else block with value " +x)
            continue@myCustomLabel
        }
    }
}

fun doubleMe(doubled: Int): Int {
    return 2*doubled;
}

fun dataTypes() {
    println("Hello, world!")

    val a: Int = 10000
    val d: Double = 100.00
    val f: Float = 100.00f
    val l: Long = 10000000004
    val s: Short = 10
    val b: Byte = 1
    val letter: Char = 'A'
    println("$letter")
    val bo: Boolean = true
    println("$bo")

    val str: String = "I'm a String \n"

    println("$str" + "!")

    val numbers: IntArray = intArrayOf(1, 2, 3, 4, 5)
    println("Hey! I'm an arr of ints: " + numbers[2])

    val numbers2: MutableList<Int> = mutableListOf(1, 2, 3)//mutable list
    val readOnlyList: List<Int> = numbers2 //immutable list
    print("my immutable list--" + numbers2 + "\n") //prints "[1,2,3]"

    numbers2.add(4)
    print("my immutable list after addition -- " + numbers2 + "\n")

    print(readOnlyList)

    val items = listOf(1, 2, 3, 4)
    println("first: " + items.first());
    println("last: " + items.last());
    println("event: " + items.filter { it % 2 == 0 });

    val readWriteMap = hashMapOf("foo" to 1, "bar" to 2)
    println(readWriteMap["foo"])

    val strings = hashSetOf("a", "b", "c", "c")
    println("My Set values:" + strings)

    //range
    val i: Int = 2
    for (j in 1..4)
        println(j)
    if (i in 1..10)
        println("we found your number -- " + i)

    val longString : String = """
        long string
        happened
        here, with big text right there
    """.trimIndent()

    println(longString)
}
