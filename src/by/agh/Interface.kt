package by.agh

fun main(args: Array<String>) {
    val obj = InterfaceImpl()

    println(
        "My variable value is = ${obj.myVar}"
    )

    println("Calling hello(): ")

    obj.sayHello()

    obj.absMethod()

    val obj2 = multipleInterfaceExample()

    obj2.printMe()
    obj2.printMeToo()

    val int = InternalExample()
    println(int.doSomething())
}

interface ExampleInterface {
    val myVar: Int //abstract property

    fun absMethod() //abstract method
    fun sayHello() {
        println("Hello there")
    } //method with default implementation
}

class InterfaceImpl : ExampleInterface {
    override val myVar: Int = 25
    override fun absMethod() {
        println("Happy learning Kotlin!")
    }

}

interface A{
    fun printMe(){
        println("It's an interface A!")
    }
}

interface B{
    fun printMeToo(){
        println("I'm from interface B")
    }
}

class multipleInterfaceExample: A,B
