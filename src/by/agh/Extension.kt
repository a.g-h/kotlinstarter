package by.agh

class Alien{
    var skills: String = "null"
    fun printMySkills(){
        println(skills)
    }
}

fun main(args: Array<String>){
    val a1 = Alien()
    a1.skills = "Java"
    a1.printMySkills()

    val a2 = Alien()
    a2.skills = "SQL"
    a2.printMySkills()

    val a3 = Alien()
    a3.skills = a1.addMySkills(a2)
    a3.printMySkills()

    println("Heyy!" + ClassWithCompanion.show())
}

fun Alien.addMySkills(a:Alien):String{
    var a4 = Alien()
    a4.skills = this.skills + "," + a.skills
    return a4.skills
}

class ClassWithCompanion{
    companion object{
        fun show(): String{
            return("You are seeing text from companion object")
        }
    }
}