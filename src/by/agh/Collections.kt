package by.agh

import java.util.*
import kotlin.collections.HashSet
import kotlin.collections.LinkedHashSet

fun main(args: Array<String>) {
    var numbers = listOf(1,2,3,4,5)
    var numbers2 : List<Int> = listOf(5,6,7)
    numbers.elementAtOrNull(3)

    val numbers3 : MutableList<Int> = mutableListOf(5,6,7)

    val items = setOf(1,2,3,4,5)
    items.minus(3) //returns without, coll remains the same
    items.plus(7) //returns with, coll remains the same

    val items2 : MutableSet<Int> = mutableSetOf(35,36,37)
    val items4 : HashSet<Int> = hashSetOf(4,5,6)
    val items5 : TreeSet<Int> = sortedSetOf(12,13,14)
    val items6 : LinkedHashSet<Int> = linkedSetOf(25,26,26)
    val items7 : MutableSet<Int> = mutableSetOf(35,36,37)

    val countries: Map<String, Int> = mapOf("USA" to 300, "France" to 60)
    val countries2 : MutableMap<String, Int> = mutableMapOf("USA" to 300, "France" to 60)

    var map1: LinkedHashMap<Int, String> = linkedMapOf(1 to "1", 2 to "2")
    var map2: HashMap<Int, String> = hashMapOf(1 to "1", 2 to "2")
    var map3: SortedMap<Int, String> = sortedMapOf(1 to "1", 2 to "2")
}