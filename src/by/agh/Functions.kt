package by.agh

fun main(args: Array<String>) {
    displayUser("Tom", 23, "Manager")
    displayUser("Alice", 21)
    displayUser("Kate")
    displayUser(name = "Garry", position = "Developer", age = 28)

    printStrings("Kotlin", "JS", "Java", "C#", "Groovy")

    printUserGroup("KT-091", "Tom", "Bob", "Alice", count = 4)

    // * - spread operator. Enable for us to pass array in vararg

    val users = arrayOf("Tom", "Bob", "Alice")
    printUserGroup("M0-011", *users, count = 3)

    val a = isFirstGreater(10.0, 10.0, 20.0, 20.0)
    val b = isFirstGreater(20.0, 20.0, 10.0, 10.0)
    println("a=$a b=$b")

    val hello = { println("hello, guys, i'm lambda func") }
    hello()
    hello()
    hello()

    run { println("hello, i'm lambda, i'm called with run") }

    val printer = { message: String -> println(message) }
    printer("Hello")
    printer("Good bye")

    val sum = { x: Int, y: Int -> println(x + y) }
    sum(2, 3)
    sum(4, 5)

    val sum2 = { x: Int, y: Int ->
        val result = x + y
        println("$x + $y = $result")
        result
    }
    val res = sum2(4, 58)
    println("again, result = $res")

    val add = { x: Int, y: Int -> x + y }
    val multiply = { x: Int, y: Int -> x * y }

    action(5, 3, add)
    action(5, 3, multiply)
    action(5, 3) { x: Int, y: Int -> x - y }
    action(5,3, fun(x:Int, y:Int): Int {return x+y})

    var action1 = selectAction(1)
    println(action1(8,4))

    action1 = selectAction(2)
    println(action1(8,7))

    var acc = Account(340)
    acc.put(300)
    println(acc.sum)
    acc put 300
    println(acc.sum)
}

//infix func
class Account(_sum: Int){
    var sum = _sum

    infix fun put(amount: Int) : Unit{
        this.sum = this.sum + amount
    }
}

fun selectAction(key: Int) : (Int, Int) -> Int{
    when(key){
        1 -> return {x:Int, y:Int -> x+y}
        2 -> return {x:Int, y:Int -> x-y}
        3 -> return {x:Int, y:Int -> x*y}
        else -> return { x:Int, y:Int -> 0}
    }
}

fun action(n1: Int, n2: Int, operation: (Int, Int) -> Int) {
    val result = operation(n1, n2)
    println(result)
}

//one-line function
fun double(x: Int) = x * x

//local func
fun isFirstGreater(base1: Double, height1: Double, base2: Double, heght2: Double): Boolean {
    fun square(base: Double, height: Double) = base * height / 2

    return square(base1, height1) > square(base2, heght2)
}

fun printUserGroup(group: String, vararg users: String, count: Int) {
    println("Group:$group")
    println("Count:$count")
    for (user in users) {
        println(user)
    }
}

fun sum(vararg numbers: Int) {
    var result = 0
    for (number in numbers) {
        result += number
    }
    println("Sum = $result")
}

fun printStrings(vararg strings: String) {
    for (string in strings) {
        println(string)
    }
}

fun displayUser(name: String, age: Int = 18, position: String = "unemployed") {
    println("Name : $name, Age: $age, Position: $position")
}
