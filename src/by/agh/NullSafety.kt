package by.agh

import java.lang.NullPointerException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull
import kotlin.test.assertTrue

fun main(args: Array<String>) {
    val p: Persona? = Persona(Country("ENG"))
    val res = p?.country?.code
    assertEquals(res, "ENG")

    val p2: Persona? = Persona(Country(null))
    val res2 = p2?.country?.code
    assertNull(res2)

    val firstName = "Tom"
    val secondName = "Michael"
    val names: List<String?> = listOf(firstName, null, secondName)

    var res3 = listOf<String?>()
    for (name in names) {
    //    name?.run { res3 = res3.plus(this) }
        name?.let { res3 = res3.plus(it) }
            ?.also { it -> println("non nullable value: $it") }
    }
    assertEquals(2, res3.size)
    assertTrue { res3.contains(firstName) }
    assertTrue { res3.contains(secondName) }

    val value: String? = null
    val res4 = value?.length ?: -1
    assertEquals(res4, -1)

    var b : String? = "value"
    b = null

    /*assertFailsWith<NullPointerException> {
        b!!.length
    }*/

    val list: List<String?> = listOf("a", null, "b")
    var result = list.filterNotNull()
    assertEquals(result.size, 2)
    assertTrue { result.contains("a") }
    assertTrue { result.contains("b") }
}

data class Persona(val country: Country?)

data class Country(val code: String?)