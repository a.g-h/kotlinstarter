package by.agh

import java.lang.Exception

fun main(args: Array<String>){
    try {
        val myVar:Int=12
        val v:String = "Kotlin"
        v.toInt()
    }catch (e:Exception){
        e.printStackTrace()
    }finally {
        println("Exception handling in Kotlin")
    }

    val a: Int? = try { factorial(5) } catch (e: Exception){ null }

    println(a)
}

fun factorial(n: Int): Int{
    if (n<1) throw Exception("Input number must be more than zero")
    var result =1
    for (i in 1..n )
        result *=i
    return result
}