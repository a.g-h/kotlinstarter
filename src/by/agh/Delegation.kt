package by.agh

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

interface Base{
    fun printMe()
}

class BaseImpl(val x: Int) : Base{
    override fun printMe() {
        println(x)
    }
}

class SecondImpl(val x: Int) : Base{
    override fun printMe() {
        println("second $x")
    }
}

class Derived(b:Base) : Base by b //delegation the public method on the object b

fun main(args: Array<String>){
    val b = BaseImpl(10)
    Derived(b).printMe() //prints 10

    val b1 = SecondImpl(10)
    Derived(b1).printMe()

    println("$myVar how you're doing?")

    val user = User()
    user.name = "first"
    user.name = "second"

    val del = DelegateExample()
    del.p = "initial value"
    del.p = "wow"

    val str = del.p
    println(str)

    //lambda
    val myLambda :(String) -> Unit = {s:String -> println(s)}
    val v:String = "Kotlinnn"
    myLambda(v)
    myFun(v, myLambda)
}

fun myFun(a: String, action: (String) -> Unit){
    println("Hey from lambdaaa!")
    action(a) // call to a lambda
}

val myVar: String by lazy {
    "Hello, guys"
}

class DelegateExample{
    var p: String by Delegate()
}

class Delegate{
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String{
        return "$thisRef, thank you for delegating '${property.name}' to me"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String)
    {
        println("'$value' has been assigned to '${property.name} in $thisRef.' ")
    }
}

class User{
    var name: String by Delegates.observable("Welcome to hell"){
            prop, oldValue, newValue ->
        println("$oldValue -> $newValue")
    }
}
