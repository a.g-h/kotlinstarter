package by.agh

class Constructors {

}

class Person(val firstName: String, var age: Int) {

}

class Human2(val firstName: String, var age: Int) {
    val message: String = "Hey!!!"

    constructor(name: String, age: Int, message: String) : this(name, age) {
    }
}

open class Parent {
    open fun think() {
        print("Hey!! I'm a child of parent and i'm thinking")
    }
}

class Child : Parent() {
    override fun think() {
        println("Hey! I've overridden parent func, and i'm thinking deeper")
    }
}

fun main(args: Array<String>) {
    val person1 = Person("Kotlin", 16)

    println("First Name = ${person1.firstName}")
    println("Age = ${person1.age}")

    val Human2 = Human2("Kotlin name", 24)

    println(
        Human2.message + Human2.firstName + " Welcome to the example of secondary constructor," +
                "Your Age is - ${Human2.age}"
    )

    var a = Child()
    a.think()

}