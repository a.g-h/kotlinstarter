package by.agh

fun main(args: Array<String>){
    val book: Book = Book("Kotlin", "John Doe", 5  )
    println("Name of book: ${book.name}")
    println("Publisher of the book: ${book.publisher}")
    println("Review score of the book: ${book.reviewScore}")

    book.reviewScore = 7
    println("All properties of the book $book")
    println("Example of hashCode ${book.hashCode()}")

    val sealed: SealedClassExample = SealedClassExample.Child1()
    val output = when(sealed){
        is SealedClassExample.Child1 -> "option one was choosen"
        is SealedClassExample.Child2 -> "option two was choosen"
    }
    println(output)
}

data class Book(val name: String, val publisher: String, var reviewScore: Int)

abstract class Resource{
    abstract var id: Long
    abstract var location: String
}

data class Kniga(
    override var id: Long = 0,
    override var location: String = "",
    var isbn: String
) : Resource ()

sealed class SealedClassExample{
    class Child1 : SealedClassExample() //SealedClassExample can be of two types only
    class Child2 : SealedClassExample()
}