package by.agh

fun main(args: Array<String>){
    val integer: Int=1
    val number: Number = integer
    println(number)

    var obj = GenericsExample<String>("Java")
    var obj1 = GenericsExample<Int>(10)
    var obj2 = GenericsExample<Double>(10.00)
    println(obj2)
}

class GenericsExample<out T>(input: T){
    init {
        println("I am getting called with the value $input")
    }
}