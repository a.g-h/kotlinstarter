package by.agh

import java.io.File
import java.nio.channels.NetworkChannel

class MyClass {
    //property
    private var name: String = "Kotlin"

    fun printMe() {
        print("You are learning $name")
    }

    class Nested {
        fun foo() = "Welcome to nested classsss"
    }
}

fun main(args: Array<String>) {
    println(MyClass.Nested().foo())

    val obj = MyClass()
    obj.printMe()

    println(Outer().Nested().foo())

    val programmer: Human = object : Human //creating an instance of the interface
    {
        override fun think() {
            println("i am an example of anonymous inner class")
        }
    }


    programmer.think()

    //Deconstructing declaration
    val s=Student("Deconstructing Declarations","Kotlin")
    val (name,subject)=s
    println("You are learning subject: $name from $subject")
}

data class Student(val a: String, val b: String) {
    var name: String = a
    var subject: String = b
}

typealias FileTable<K> = MutableMap<K, MutableList<File>>

interface Human {
    fun think()
}

class Outer {
    private val welcomeMessage: String = "\nWelcome to kotlin"

    inner class Nested {
        fun foo() = welcomeMessage
    }
}