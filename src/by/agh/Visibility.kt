package by.agh

import kotlin.reflect.jvm.internal.impl.protobuf.Internal

private class privateClass {
    private val i = 1

    private fun doSomething() {
        println(i)
    }
}

fun main(args: Array<String>) {
    val p = PJ()
    println(p.getValue())

    val int = InternalExample()
    println(int.doSomething())
}

open class P {
    protected val i = 1
}

class PJ : P() {
    fun getValue(): Int {
        return i
    }
}

class InternalExample {
    internal val i = 1

    internal fun doSomething(){
        println(i)
    }
}